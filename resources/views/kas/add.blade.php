@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold">Tambah Kas</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="col-12">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="col-12">
                            <form action="{{ url('/kas') }}" method="POST">
                                @csrf
                                @method('POST')
                                <div class="form-group">
                                    <label for="price" class="font-weight-bold my-1">Nominal</label>
                                    <input type="number" class="form-control" name="price" id="price">
                                </div>
                                <div class="form-group">
                                    <label for="type" class="font-weight-bold my-1">Tipe</label>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">--- Select ---</option>
                                        <option value="masuk">Masuk</option>
                                        <option value="keluar">Keluar</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="remark" class="font-weight-bold my-1">Keterangan</label>
                                    <input type="text" class="form-control" name="remark" id="remark">
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('/kas') }}" class="btn btn-danger">Kembali</a>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
