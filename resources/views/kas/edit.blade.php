@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold">Edit Daftar Barang</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="col-12">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="col-12">
                            <form action="{{ url("/kas/$kas->id") }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="price" class="font-weight-bold my-1">Nominal</label>
                                    <input type="number" class="form-control" value="{{ $kas->price }}" name="price" id="price">
                                </div>
                                <div class="form-group">
                                    <label for="type" class="font-weight-bold my-1">Tipe</label>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">--- Select ---</option>
                                        <option value="masuk" @if($kas->type === "masuk") selected @endif>Masuk</option>
                                        <option value="keluar" @if($kas->type === "keluar") selected @endif>Keluar</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="remark" class="font-weight-bold my-1">Keterangan</label>
                                    <input type="text" class="form-control" value="{{ $kas->remark }}" name="remark" id="remark">
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('/kas') }}" class="btn btn-danger">Kembali</a>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
