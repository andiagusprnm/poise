@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold"> Daftar Customer</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row my-2">
                        <div class="col-12">
                            <a href="{{ url('/customer/tambah') }}" class="btn btn-primary">Tambah Customer</a>
                            @if (session('message'))
                                <div class="alert alert-success my-1">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Nama</th>
                                    <th>Keterangan</th>
                                    <th>Tanggal Buat</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($customers as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->code }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->keterangan }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        <td class="d-flex">
                                            <a href="{{ url("/customer/edit/$data->id") }}" class="btn btn-primary">Edit</a>
                                            @if (auth()->user()->role == 1)
                                                <form action="{{ url("/customer/$data->id") }}" method="POST">
                                                    @csrf
                                                    @method("DELETE")
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
