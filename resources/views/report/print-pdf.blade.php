<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    td {
        padding: 20px;
        margin: 5px;
    }

    .center {
        text-align: center;
    }
</style>
<header class="center">
    <h3>Laporan Barang {{ $type }}</h3>
    <p>{{ $from }} - {{ $to }}</p>
</header>
<div class="center">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Code</th>
                <th>Nama</th>
                <th>Tipe</th>
                <th>Harga</th>
                <th>Qty</th>
                <th>Jumlah</th>
                <th>Tanggal Barang Keluar</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($list as $data)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $data->barang->code }}</td>
                    <td>{{ $data->barang->name }}</td>
                    <td>{{ $data->barang->type_barang->name }}</td>
                    <td>{{ number_format($data->barang->price, 2, ',', '.') }}</td>
                    <td>{{ $data->qty }}</td>
                    <td>{{ number_format($data->qty * $data->barang->price, 2, ',', '.') }}</td>
                    <td>{{ $data->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
