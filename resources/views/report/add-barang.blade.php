@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold">Tambah Daftar Barang</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="col-12">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="col-12">
                            <form action="{{ url('/laporan-barang?t=' . $param) }}" method="POST">
                                @csrf
                                @method('POST')
                                <div class="form-group">
                                    <label for="barang_id" class="font-weight-bold my-1">Barang</label>
                                    <select name="barang_id" id="barang_id" class="form-control"
                                        value="{{ old('barang_id') }}">
                                        <option value="">Select</option>
                                        @foreach ($barang as $data)
                                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="qty" class="font-weight-bold my-1">Qty</label>
                                    <input type="number" class="form-control" name="qty" id="qty"
                                        value="{{ old('qty') }}">
                                </div>
                                <div class="form-group">
                                    <label for="type" class="font-weight-bold my-1">Type</label>
                                    <select name="type" id="type" class="form-control" value="{{ old('type') }}">

                                        <option value="1" @if ($param === 'm') selected @endif>Masuk
                                        </option>
                                        <option value="2" @if ($param === 'k') selected @endif>Keluar
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <a href="{{ url('/laporan-barang?t=' . $param) }}" class="btn btn-danger">Kembali</a>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
