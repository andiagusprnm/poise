@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold"> Daftar Barang Keluar</h4>
                </div>
                <div class="card-body shadow">
                    <form action="/laporan-barang/print" method="GET">
                        <div class="row my-2">
                            <div class="col-12">
                                <a href="{{ url('/laporan-barang/tambah?t=' . $param) }}" class="btn btn-primary">Tambah
                                    Barang
                                    Keluar</a>
                                <button class="btn btn-primary" type="submit">Download PDF</button>
                            </div>
                            <div class="col-12 col-md-6 my-2">
                                <input type="hidden" value="{{ $param }}" name="t">
                                <label for="">Dari</label>
                                <input type="datetime-local" name="from" required class="form-control">
                            </div>
                            <div class="col-12 col-md-6 my-2">
                                <label for="">Sampai</label>
                                <input type="date" required name="to" class="form-control">
                            </div>
                            @if (session('message'))
                                <div class="col-12">
                                    <div class="alert alert-success my-1">
                                        {{ session('message') }}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Nama</th>
                                    <th>Tipe</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal Barang Keluar</th>
                                    @if (auth()->user()->role == 1)
                                            <th>
                                                Opsi
                                            </th>
                                        @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($list as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->barang->code }}</td>
                                        <td>{{ $data->barang->name }}</td>
                                        <td>{{ $data->barang->type_barang->name }}</td>
                                        <td>{{ number_format($data->barang->price, 2, ',', '.') }}</td>
                                        <td>{{ $data->qty }}</td>
                                        <td>{{ number_format($data->qty * $data->barang->price, 2, ',', '.') }}</td>
                                        <td>{{ $data->created_at }}</td>
                                        @if (auth()->user()->role == 1)
                                            <td>
                                                <form action="{{ url("/laporan-barang/$data->id?t=$param") }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
