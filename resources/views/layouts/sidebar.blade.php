<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Poise</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/barang') }}">
            <i class="fas fa-solid fa-box"></i>
            <span>Barang</span></a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/kategori-barang') }}">
            <i class="fas fa-solid fa-list"></i>
            <span>Kategori</span></a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/customer') }}">
            <i class="fas fa-solid fa-address-book"></i>
            <span>Customer</span></a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href="{{ url('/kas') }}">
            <i class="fas fa-solid fa-calculator"></i>
            <span>Kas</span></a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-solid fa-receipt"></i>
            <span>Laporan Barang</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Laporan Barang:</h6>
                <a class="collapse-item" href="{{ url('/laporan-barang?t=m') }}">Masuk</a>
                <a class="collapse-item" href="{{ url('/laporan-barang?t=k') }}">Keluar</a>
            </div>
        </div>
    </li>

    @if (auth()->user()->role == 1)
        <li class="nav-item active">
            <a class="nav-link" href="{{ url('/user-management') }}">
                <i class="fas fa-solid fa-calculator"></i>
                <span>User Management</span></a>
        </li>
    @endif

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
