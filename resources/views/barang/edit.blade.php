@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold">Edit Daftar Barang</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="col-12">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="col-12">
                            <form action="{{ url('/barang') }}/{{$barang->id}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="code" class="font-weight-bold my-1">Code</label>
                                    <input type="text" class="form-control" value="{{ $barang->code }}" name="code"
                                        id="code">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold my-1">Nama</label>
                                    <input type="text" class="form-control" value="{{ $barang->name }}" name="name"
                                        id="name">
                                </div>
                                <div class="form-group">
                                    <label for="price" class="font-weight-bold my-1">Harga</label>
                                    <input type="number" class="form-control" value="{{ $barang->price }}" name="price"
                                        id="price">
                                </div>
                                <div class="form-group">
                                    <label for="type_barang_id" class="font-weight-bold my-1">Tipe Barang</label>
                                    <select value="{{ $barang->type_barang_id }}" name="type_barang_id" id="type_barang_id"
                                        class="form-control">
                                        <option value="{{ $barang->type_barang_id }}">{{ $barang->type_barang->name }}
                                        </option>
                                        @foreach ($types as $type)
                                            @if ($type->id !== $barang->type_barang_id)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('/barang') }}" class="btn btn-danger">Kembali</a>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
