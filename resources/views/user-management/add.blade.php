@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold">Tambah Daftar Barang</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="col-12">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="col-12">
                            <form action="{{ url('/user-management') }}" method="POST">
                                @csrf
                                @method('POST')
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold my-1">Name</label>
                                    <input type="text" class="form-control" value="{{ old('name') }}" name="name"
                                        id="name">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="font-weight-bold my-1">Email</label>
                                    <input type="text" class="form-control" value="{{ old('email') }}" name="email"
                                        id="email">
                                </div>
                                <div class="form-group">
                                    <label for="password" class="font-weight-bold my-1">Password</label>
                                    <input type="password" class="form-control" value="{{ old('password') }}"
                                        name="password" id="password">
                                </div>
                                <div class="form-group">
                                    <label for="confirm_password" class="font-weight-bold my-1">Confirm Password</label>
                                    <input type="password" class="form-control"
                                        value="{{ old('confirm_password') }}" name="confirm_password" id="confirm_password">
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="font-weight-bold my-1">Phone</label>
                                    <input type="number" class="form-control" value="{{ old('phone') }}" name="phone"
                                        id="phone">
                                </div>
                                <div class="form-group">
                                    <label for="role" class="font-weight-bold my-1">Role</label>
                                    <select name="role" id="role" class="form-control">
                                        <option value="1">Admin</option>
                                        <option value="2">Kasir</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('/kategori-barang') }}" class="btn btn-danger">Kembali</a>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
