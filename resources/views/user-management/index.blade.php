@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold">Daftar User</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row my-2">
                        <div class="col-12">
                            <a href="{{ url('/user-management/tambah') }}" class="btn btn-primary">Tambah User</a>
                            @if (session('message'))
                                <div class="alert alert-success my-1">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Role</th>
                                    <th>Tanggal Buat</th>
                                    <th>Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($list as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->name }}</td>
                                        <td>{{ $data->email }}</td>
                                        <td>{{ $data->phone }}</td>
                                        @if ($data->role == 1)
                                            <td>
                                                Admin
                                            </td>
                                        @else
                                            <td>
                                                Kasir
                                            </td>
                                        @endif
                                        <td>{{ $data->created_at }}</td>
                                        @if (auth()->user()->role == 1)
                                            <td>
                                                <form action="{{ url("/user-management/$data->id") }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
