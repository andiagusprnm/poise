@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="font-weight-bold">Edit Daftar Barang</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row">
                        @if (count($errors) > 0)
                            <div class="col-12">
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger">{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        <div class="col-12">
                            <form action="{{ url('/kategori-barang/') }}/{{$data->id}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="code" class="font-weight-bold my-1">Code</label>
                                    <input type="text" class="form-control" value="{{ $data->code }}" name="code"
                                        id="code">
                                </div>
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold my-1">Nama</label>
                                    <input type="text" class="form-control" value="{{ $data->name }}" name="name"
                                        id="name">
                                </div>
                                <div class="form-group">
                                    <a href="{{ url('/kategori-barang') }}" class="btn btn-danger">Kembali</a>
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
