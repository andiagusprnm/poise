@extends('layouts.layout')

@section('main-content')
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4>Barang Masuk Hari Ini</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row my-2">
                        <div class="col-12">
                            <a href="{{ url('/laporan-barang?t=m') }}" class="btn btn-primary">Ke Halaman</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Nama</th>
                                    <th>Tipe</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal Barang Keluar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($barang_in) > 0)
                                    @foreach ($barang_in as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->barang->code }}</td>
                                            <td>{{ $data->barang->name }}</td>
                                            <td>{{ $data->barang->type_barang->name }}</td>
                                            <td>{{ number_format($data->barang->price, 2, ',', '.') }}</td>
                                            <td>{{ $data->qty }}</td>
                                            <td>{{ number_format($data->qty * $data->barang->price, 2, ',', '.') }}</td>
                                            <td>{{ $data->created_at }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center">Tidak ada barang masuk hari ini</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4>Barang Keluar Hari Ini</h4>
                </div>
                <div class="card-body shadow">
                    <div class="row my-2">
                        <div class="col-12">
                            <a href="{{ url('/laporan-barang?t=k') }}" class="btn btn-primary">Ke Halaman</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Code</th>
                                    <th>Nama</th>
                                    <th>Tipe</th>
                                    <th>Harga</th>
                                    <th>Qty</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal Barang Keluar</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($barang_out) > 0)
                                    @foreach ($barang_out as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->barang->code }}</td>
                                            <td>{{ $data->barang->name }}</td>
                                            <td>{{ $data->barang->type_barang->name }}</td>
                                            <td>{{ number_format($data->barang->price, 2, ',', '.') }}</td>
                                            <td>{{ $data->qty }}</td>
                                            <td>{{ number_format($data->qty * $data->barang->price, 2, ',', '.') }}</td>
                                            <td>{{ $data->created_at }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center">Tidak ada barang keluar hari ini</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
