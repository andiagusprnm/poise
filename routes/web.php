<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\BarangMasukKeluarController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KasController;
use App\Http\Controllers\TypeBarangController;
use App\Http\Controllers\UserManagementController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::controller(AuthController::class)->group(function () {
  Route::get('/login', "index");
  Route::post('/login', "store");
  Route::get('/logout', "logout");
});

Route::middleware("login.access")->group(function () {
  Route::controller(HomeController::class)->group(function () {
    Route::get("/", "index");
  });

  Route::controller(BarangController::class)->group(function () {
    Route::get("/barang", "index");
    Route::get("/barang/tambah", "add");
    Route::post("/barang", "store");
    Route::get("/barang/edit/{id}", "edit");
    Route::put("/barang/{id}", "storeEdit");
    Route::delete("/barang/{id}", "destroy");
  });

  Route::controller(TypeBarangController::class)->group(function() {
    Route::get("/kategori-barang", "index");
    Route::post("/kategori-barang", "store");
    Route::get("/kategori-barang/tambah", "add");
    Route::get("/kategori-barang/edit/{id}", "edit");
    Route::put("/kategori-barang/{id}", "storeEdit");
    Route::delete("/kategori-barang/{id}", "destroy");
  });

  Route::controller(BarangMasukKeluarController::class)->group(function() {
    Route::get("/laporan-barang", "index");
    Route::post("/laporan-barang", "store");
    Route::get("/laporan-barang/tambah", "add");
    Route::get("/laporan-barang/print", "print");
    Route::delete("/laporan-barang/{id}", "destroy");
  });

  Route::controller(CustomerController::class)->group(function() {
    Route::get("/customer", "index");
    Route::post("/customer", "store");
    Route::get("/customer/tambah", "add");
    Route::get("/customer/edit/{id}", "edit");
    Route::put("/customer/{id}", "storeEdit");
    Route::delete("/customer/{id}", "destroy");
  });

  Route::controller(KasController::class)->group(function() {
    Route::get("/kas", "index");
    Route::post("/kas", "store");
    Route::get("/kas/tambah", "add");
    Route::get("/kas/edit/{id}", "edit");
    Route::put("/kas/{id}", "storeEdit");
    Route::delete("/kas/{id}", "destroy");
  });

  Route::controller(UserManagementController::class)->group(function() {
    Route::get("/user-management", "index");
    Route::post("/user-management", "store");
    Route::get("/user-management/tambah", "add");
    Route::delete("/user-management/{id}", "destroy");
  });


  
  // Route::get("/barang", [BarangController::class, "index"]);
  // Route::get("/barang/tambah", [BarangController::class, "add"]);
  // Route::get("/barang", [BarangController::class, "index"]);
  // Route::get("/barang", [BarangController::class, "index"]);
});
