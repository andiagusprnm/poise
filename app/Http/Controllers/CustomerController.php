<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = Customer::orderBy("id", "DESC")->get();
        return view("customer.index", compact("customers"));
    }

    public function add()
    {
        return view("customer.add");
    }

    public function edit(Request $request, $id)
    {
        $customer = Customer::where("id", $id)->first();
        return view("customer.edit", compact("customer"));
    }

    public function store(Request $request)
    {
        $request->validate([
            "code" => "required",
            "name" => "required",
            "keterangan" => "required",
        ]);
        $customer = Customer::create($request->all());
        $customer->save();

        return redirect("/customer")->with("message", "Berhasil simpan data");
    }

    public function storeEdit(Request $request, $id)
    {
        $request->validate([
            "code" => "required",
            "name" => "required",
            "keterangan" => "required",
        ]);

        $save = [
            "code" => $request->code,
            "name" => $request->name,
            "keterangan" => $request->keterangan,
        ];
        Customer::where("id", $id)->update($save);
        return redirect("/customer")->with("message", "Berhasil merubah data");
    }

    public function destroy(Request $request, $id) {
        $result = Customer::destroy($id);

        if ($result) {
            return redirect("/customer")->with("message", "Berhasil hapus data");
        }
        
        return redirect("/customer")->with("message", "Gagal hapus data");
    }
}
