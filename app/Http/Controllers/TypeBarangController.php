<?php

namespace App\Http\Controllers;

use App\Models\TypeBarang;
use Illuminate\Http\Request;

class TypeBarangController extends Controller
{
    public function index()
    {
        $list = TypeBarang::orderBy("id", "desc")->get();
        return view("type-barang.index", ["list" => $list]);
    }

    public function add()
    {
        return view("type-barang.add");
    }

    public function edit(Request $request, $id)
    {
        $data = TypeBarang::where("id", $id)->first();
        return view("type-barang.edit", ["data" => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "code" => "required",
            "name" => "required",
        ], [
            "code.required" => "The Code field is required",
            "name.required" => "The Nama field is required",
        ]);
        $type = TypeBarang::create($request->all());
        $type->save();
        return redirect("/kategori-barang")->with("message", "Berhasil simpan data");
    }

    public function storeEdit(Request $request, $id)
    {
        $request->validate([
            "code" => "required",
            "name" => "required",
        ]);

        $save = [
            "code" => $request->code,
            "name" => $request->name,
        ];
        TypeBarang::where('id', $id)->update($save);
        return redirect("/kategori-barang")->with("message", "Berhasil merubah data");
    }

    public function destroy(Request $request, $id) {
        $result = TypeBarang::destroy($id);

        if ($result) {
            return redirect("/kategori-barang")->with("message", "Berhasil hapus data");
        }
        
        return redirect("/kategori-barang")->with("message", "Gagal hapus data");
    }
}
