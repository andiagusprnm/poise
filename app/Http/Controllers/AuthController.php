<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function index()
    {
        return view("login.index");
    }

    public function store(Request $request)
    {
        $credentials = $request->validate([
            "email" => "required",
            "password" => "required",
        ]);
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect("/");
        } else {
            return redirect("login")->with("failed", "Email atau password salah");
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect("/login");
    }
}
