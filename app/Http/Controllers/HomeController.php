<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangMasukKeluar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
        $barang_in = BarangMasukKeluar::with("barang")
            ->where("type", 1)
            ->whereDate("created_at", Carbon::now()->format("Y-m-d"))
            ->get();

        $barang_out = BarangMasukKeluar::with("barang")
            ->where("type", 2)
            ->whereDate("created_at", Carbon::now()->format("Y-m-d"))
            ->get();
            
        return view("home.index", compact("barang_in", "barang_out"));
    }
}
