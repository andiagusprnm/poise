<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangMasukKeluar;
use Illuminate\Http\Request;

use PDF;

class BarangMasukKeluarController extends Controller
{
    public function index(Request $request)
    {
        $where = $request->query("t") == "m" ? 1 : 2;
        $view = $request->query("t") == "m" ? "report.barang-masuk" : "report.barang-keluar";
        $param = $request->query("t");
        
        $list = BarangMasukKeluar::with("barang")->where("type", $where)->orderBy("id", "DESC")->get();

        return view($view, compact("list", "param"));
    }

    public function add(Request $request)
    {
        $barang = Barang::all(["id", "name"]);
        $param = $request->query("t");
        return view("report.add-barang", compact("barang", "param"));
    }

    public function store(Request $request)
    {
        $request->validate([
            "barang_id" => "required",
            "qty" => "required",
            "type" => "required",
        ]);
        $bmk = BarangMasukKeluar::create($request->all());
        $bmk->save();

        return redirect("/laporan-barang?t=" . $request->query("t"))->with("message", "Berhasil simpan data");
    }

    public function destroy(Request $request, $id) {
        $result = BarangMasukKeluar::destroy($id);

        if ($result) {
            return redirect("/laporan-barang?t=" . $request->query("t"))->with("message", "Berhasil hapus data");
        }
        
        return redirect("/laporan-barang?t=" . $request->query("t"))->with("message", "Berhasil hapus data");
    }

    public function print(Request $request)
    {
        $where = 2;
        $type = "Keluar";
        if ($request->query("t") == "m") {
            $where = 1;
            $type = "Masuk";
        }
        $to = $request->query("to");
        $from  = $request->query("from");
        $list = BarangMasukKeluar::with("barang")->where("type", $where)->whereBetween("created_at", [$from, $to])->get();

        $pdf = PDF::loadView("report.print-pdf", [
            "list" => $list,
            "type" => $type,
            "from" => str_replace("T", " ", $from),
            "to" => str_replace("T", " ", $to),
        ]);
        $pdf->stream("doc.pdf");
        // return view(, );
    }
}
