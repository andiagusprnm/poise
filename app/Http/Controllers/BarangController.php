<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\TypeBarang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    public function index()
    {
        $list = Barang::with("type_barang")->orderBy("id", "desc")->get();

        return view("barang.index", ["list" => $list]);
    }

    public function add()
    {
        $type = TypeBarang::all(["id", "code", "name"]);
        return view("barang.add", [
            "types" => $type
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "code" => "required",
            "name" => "required",
            "price" => "required",
            "type_barang_id" => "required",
        ]);

        $barang = Barang::create($request->all());
        $barang->save();
        return redirect("/barang")->with("message", "Berhasil simpan data");
    }

    public function storeEdit(Request $request, $id)
    {
        $request->validate([
            "code" => "required",
            "name" => "required",
            "price" => "required",
            "type_barang_id" => "required",
        ]);

        $save = [
            "code" => $request->code,
            "name" => $request->name,
            "price" => $request->price,
            "type_barang_id" => $request->type_barang_id,
        ];
        Barang::where('id', $id)->update($save);
        return redirect("/barang")->with("message", "Berhasil merubah data");
    }

    public function edit(Request $request, $id)
    {
        $barang = Barang::where("id", $id)->first();
        $type = TypeBarang::all(["id", "code", "name"]);
        return view("barang.edit", [
            "barang" => $barang,
            "types" => $type
        ]);
    }

    public function destroy(Request $request, $id) {
        $result = Barang::destroy($id);

        if ($result) {
            return redirect("/barang")->with("message", "Berhasil hapus data");
        }
        
        return redirect("/barang")->with("message", "Gagal hapus data");
    }
}
