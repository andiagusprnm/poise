<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserManagementController extends Controller
{
    public function index()
    {
        $list = User::orderBy("id", "DESC")->get(["id", "name", "email", "role", "created_at", "phone"]);
        return view("user-management.index", compact("list"));
    }

    public function add()
    {
        return view("user-management.add");
    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
            "email" => "required",
            "role" => "required",
            "password" => "required|min:6",
            "confirm_password" => "required|min:6|same:password",
            "phone" => "required",
        ]);
        $request["password"] = bcrypt($request->password);
        $user = User::create($request->all());
        $user->save();
        return redirect("/user-management")->with("message", "Berhasil simpan data");
    }

    public function destroy(Request $request, $id) {
        $result = User::destroy($id);
        if ($result) {
            return redirect("/user-management")->with("message", "Berhasil hapus data");
        }
        
        return redirect("/user-management")->with("message", "Gagal hapus data");
    }
}
