<?php

namespace App\Http\Controllers;

use App\Models\Kas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KasController extends Controller
{
    public function index()
    {
        $list = Kas::with("user")->orderBy("id", "desc")->get();

        return view("kas.index", compact("list"));
    }

    public function add()
    {
        return view("kas.add");
    }

    public function store(Request $request)
    {
        $request->validate([
            "price" => "required",
            "type" => "required",
            "remark" => "required",
        ]);
        $data = $request->all();
        $data["created_by"] = Auth::user()->id;
        
        $kas = Kas::create($data);
        $kas->save();
        return redirect("/kas")->with("message", "Berhasil simpan data");
    }

    public function storeEdit(Request $request, $id)
    {
        $request->validate([
            "type" => "required",
            "price" => "required",
            "remark" => "required",
        ]);

        $save = [
            "price" => $request->price,
            "remark" => $request->remark,
            "type" => $request->type,
            "created_by" => Auth::user()->id,
        ];
        Kas::where('id', $id)->update($save);
        return redirect("/kas")->with("message", "Berhasil merubah data");
    }

    public function edit(Request $request, $id)
    {
        $kas = Kas::where("id", $id)->first();
        return view("kas.edit", compact("kas"));
    }

    public function destroy(Request $request, $id) {
        $result = Kas::destroy($id);

        if ($result) {
            return redirect("/kas")->with("message", "Berhasil hapus data");
        }
        
        return redirect("/kas")->with("message", "Gagal hapus data");
    }
}
