<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kas extends Model
{
    use HasFactory;

    protected $table = "kas";

    protected $fillable = [
        "type",
        "price",
        "remark",
        "created_by",
    ];

    public function user()
    {
        return $this->belongsTo(User::class, "created_by");
    }
}
