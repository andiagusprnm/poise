<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangMasukKeluar extends Model
{
    use HasFactory;

    protected $table = "barang_masuk_keluar";

    protected $fillable = [
        'barang_id',
        'qty',
        'type'
    ];

    public function barang()
    {
        return $this->belongsTo(Barang::class);
    }
}
