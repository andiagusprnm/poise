<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $table = "barang";

    protected $fillable = [
        'name',
        'code',
        'price',
        'type_barang_id'
    ];

    public function type_barang()
    {
        return $this->belongsTo(TypeBarang::class);
    }

    public function barang_masuk_keluar()
    {
        return $this->hasMany(BarangMasukKeluar::class);
    }
}
