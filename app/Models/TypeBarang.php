<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeBarang extends Model
{
    use HasFactory;

    protected $table = "type_barang";

    protected $fillable = [
        'name',
        'code',
    ];

    public function barang()
    {
        return $this->hasMany(Barang::class);
    }
}
